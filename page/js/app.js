var shiponTaxi = angular.module('shiponTaxi', ['ui.bootstrap', 'ui.router']);

shiponTaxi.config(function($stateProvider, $urlRouterProvider) {
    
    $stateProvider
    .state({
        name: 'about',
        url: '/about',
        templateUrl: 'page/templates/about.html'
    })
    .state({
        name: 'cities',
        url: '/cities',
        templateUrl: 'page/templates/cities.html'
    })
    .state({
        name: 'contacts',
        url: '/contacts',
        templateUrl: 'page/templates/contacts.html'
    });
    
    $urlRouterProvider.otherwise('/about');
    
});

shiponTaxi.controller('appController', ['$location', function($location) {
    
    var appCtrl = this;
    
    appCtrl.isPath = function(path) {
        return path === $location.path();
    };
    
    appCtrl.getPath = function(path) {
        return "page/" + path;
    };
    
}]);
